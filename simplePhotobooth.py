#!/usr/bin/python3
import sys
import os
import pyudev
import psutil

from PIL import Image  # image manipulation for Overlay
import time  # timing
import picamera  # camera driver
import shutil  # file io access like copy
from datetime import datetime  # datetime routine
import RPi.GPIO as GPIO  # gpio access
import subprocess  # call external scripts
from transitions import Machine  # state machine
from transitions.extensions.states import add_state_features, Timeout
import configparser  # parsing config file
import logging  # logging functions
import cups  # connection to cups printer driver
import usb  # check if printer is connected and turned on
from wand.image import Image as image  # image manipulation lib
from os import listdir
from os.path import isfile, join
import random

# get the real path of the script
REAL_PATH = os.path.dirname(os.path.realpath(__file__))

@add_state_features(Timeout)
class CustomStateMachine(Machine):
    pass

"""
Class controlling the photobooth
"""

class Photobooth:
    # define state machine for taking photos
    FSMstates = ['PowerOn', 
        {'name': 'Start', 'timeout': 20, 'on_timeout': 'PhotoShowTimeout'},
        'CountdownPhoto', 
        'TakePhoto', 
        {'name': 'ShowPrintPhoto', 'timeout': 60, 'on_timeout': 'ShowTimeout'},
        {'name': 'ShowPhoto', 'timeout': 10, 'on_timeout': 'ShowTimeout'},
        {'name': 'PhotoShow', 'timeout': 10, 'on_timeout': 'PhotoShowTimeout'},
        'PrintPhoto',
        'RefillPaper', 
        'RefillInk', 
        'Restart']

    def __init__(self):
        # read config
        logging.debug("Read Config File")
        self.readConfiguration()
        
        logging.debug("Init state machine")
        self.initStateMachine()
        
        self.photo_taken = False

        logging.debug("Config GPIO")
        GPIO.setwarnings(False)
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(self.pin_button_up, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        GPIO.setup(self.pin_button_down, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        GPIO.add_event_detect(self.pin_button_up, GPIO.FALLING, callback=self.Button1pressed, bouncetime=500)
        GPIO.add_event_detect(self.pin_button_down, GPIO.FALLING, callback=self.Button2pressed, bouncetime=500)

        logging.debug("Set TimeStamp for Buttons")
        self.time_stamp_button1 = time.time()
        self.time_stamp_button2 = time.time()

        self.button1active = False
        self.button2active = False

        logging.debug("Setup Camera")
        # Setup Camera
        try:
            self.camera = picamera.PiCamera()
        except:
            logging.CRITICAL("error initializing the camera - exiting")
            raise SystemExit

        self.camera.resolution = (self.photo_w, self.photo_h)
        self.camera.hflip = self.flip_screen_h
        self.camera.vflip = self.flip_screen_v
        self.startpreview()

        self.cycleCounter = 0

        # load the Logo of the Photobooth and display it
        self.overlayscreen_logo = self.overlay_image_transparency(self.screen_logo, 0, 5)

        # find the USB Drive, if connected
        self.PhotoCopyPath = self.GetMountpoint()

        # path for saving photos on usb drive
        if self.PhotoCopyPath is not None:
            self.PhotoCopyPath = self.PhotoCopyPath + "/Fotos"
            logging.debug("Photocopypath = " + self.PhotoCopyPath)
            if not os.path.exists(self.PhotoCopyPath):
                logging.debug("os.mkdir(self.PhotoCopyPath)")
                os.mkdir(self.PhotoCopyPath)
        else:
            logging.debug("self.PhotoCopyPath not Set -> No USB Drive Found")

        # Start the Application
        self.on_enter_PowerOn()

    # ends the Application
    def __del__(self):
        logging.debug("__del__ Function")
        self.stoppreview()
        self.camera.close()
        GPIO.setmode(GPIO.BCM)
        GPIO.cleanup()

    # Init the State machine controlling the Photobooth
    def initStateMachine(self):
        logging.debug("Init State Machine")
        self.machine = CustomStateMachine(model=self, states=self.FSMstates, initial='PowerOn', ignore_invalid_triggers=True)
        self.machine.add_transition(source='PowerOn', dest='PowerOn',
                                    trigger='Button1')  # power on self test - check if printer is conected
        self.machine.add_transition(source='PowerOn', dest='Start',
                                    trigger='PrinterFound')  # printer is on -> goto start
        self.machine.add_transition(source='Start', dest='CountdownPhoto', trigger='Button1')
        self.machine.add_transition(source='Start', dest='CountdownPhoto', trigger='Button2')
        self.machine.add_transition(source='Start', dest='PhotoShow', trigger='PhotoShowTimeout', conditions='is_photo_taken', after='after_Start_to_PhotoShow')
        self.machine.add_transition(source='CountdownPhoto', dest='TakePhoto',
                                    trigger='CountdownPhotoTimeout')  # timeout
        if self.printPicsEnable == True:
            self.machine.add_transition(source='TakePhoto', dest='ShowPrintPhoto', trigger='None')
            self.machine.add_transition(source='ShowPrintPhoto', dest='PrintPhoto', trigger='Button1')  # print
            self.machine.add_transition(source='ShowPrintPhoto', dest='Restart', trigger='Button2')  # do not print
            self.machine.add_transition(source='ShowPrintPhoto', dest='Restart', trigger='ShowTimeout') # do not print
        else:
            self.machine.add_transition(source='TakePhoto', dest='ShowPhoto', trigger='None')
            self.machine.add_transition(source='ShowPhoto', dest='CountdownPhoto', trigger='Button1')  # print
            self.machine.add_transition(source='ShowPhoto', dest='CountdownPhoto', trigger='Button2')  # do not print
            self.machine.add_transition(source='ShowPhoto', dest='Restart', trigger='ShowTimeout') # do not print
            
        self.machine.add_transition(source='PhotoShow', dest='PhotoShow', trigger='PhotoShowTimeout') # show next photo
        self.machine.add_transition(source='PhotoShow', dest='CountdownPhoto', trigger='Button1', before='before_PhotoShow_to_Countdown')  # print
        self.machine.add_transition(source='PhotoShow', dest='CountdownPhoto', trigger='Button2', before='before_PhotoShow_to_Countdown')  # do not print
        
        self.machine.add_transition(source='PrintPhoto', dest='Restart', trigger='PrintDone')  # print done

        self.machine.add_transition(source='RefillPaper', dest='Restart',
                                    trigger='Button1')  # Refill Paper on printer
        self.machine.add_transition(source='RefillPaper', dest='Restart',
                                    trigger='Button2')  # Refill Paper on printer
        self.machine.add_transition(source='RefillInk', dest='Start',
                                    trigger='Button1')  # Refill Ink on printer
        self.machine.add_transition(source='RefillInk', dest='Start',
                                    trigger='Button2')  # Refill Ink on printer
        self.machine.add_transition(source='PrintPhoto', dest='RefillPaper',
                                    trigger='PaperEmpty')  # Refill Paper on printer
        self.machine.add_transition(source='PrintPhoto', dest='RefillInk',
                                    trigger='InkEmpty')  # Refill Ink on printer

    # read the global configuration, folders, resolution....
    def readConfiguration(self):
        logging.debug("Read Config File")
        self.config = configparser.ConfigParser()
        self.config.sections()
        self.config.read(os.path.join(REAL_PATH, 'config.ini'))

        if self.config.getboolean("Debug", "debug", fallback=True) == True:
            logging.basicConfig(level=logging.DEBUG)
        else:
            logging.basicConfig(level=logging.WARNING)

        self.printPicsEnable = self.config.getboolean("Debug", "print", fallback=True)

        if self.printPicsEnable == False:
            logging.debug("Printing pics disabled")

        date_string = ((str(datetime.now()).split('.')[0]).replace(' ', '_')).replace(':', '-')
        self.photo_abs_file_path = os.path.join(REAL_PATH, self.config.get("Paths", "photo_path", fallback="Photos/") + date_string + "/")
        os.mkdir(self.photo_abs_file_path)
        self.screens_abs_file_path = os.path.join(REAL_PATH,
                                                  self.config.get("Paths", "screen_path", fallback="Screens/"))
        self.pin_button_up = int(self.config.get("InOut", "pin_button_left", fallback="23"))
        self.pin_button_down = int(self.config.get("InOut", "pin_button_right", fallback="24"))
        self.photo_w = int(self.config.get("Resolution", "photo_w", fallback="3280"))
        self.photo_h = int(self.config.get("Resolution", "photo_h", fallback="2464"))
        self.screen_w = int(self.config.get("Resolution", "screen_w", fallback="1024"))
        self.screen_h = int(self.config.get("Resolution", "screen_h", fallback="600"))
        self.flip_screen_h = self.config.getboolean("Resolution", "flip_screen_h", fallback=False)
        self.flip_screen_v = self.config.getboolean("Resolution", "flip_screen_v", fallback=False)
        self.screen_turnOnPrinter = os.path.join(self.screens_abs_file_path,
                                                 self.config.get("Screens", "screen_turn_on_printer",
                                                                 fallback="ScreenTurnOnPrinter.png"))
        self.screen_logo = os.path.join(self.screens_abs_file_path,
                                        self.config.get("Screens", "screen_logo", fallback="ScreenLogo.png"))
        self.screen_start = os.path.join(self.screens_abs_file_path,
                                                 self.config.get("Screens", "screen_start",
                                                                 fallback="ScreenStart.png"))
        self.screen_countdown_0 = os.path.join(self.screens_abs_file_path,
                                               self.config.get("Screens", "screen_countdown_0",
                                                               fallback="ScreenCountdown0.png"))
        self.screen_countdown_1 = os.path.join(self.screens_abs_file_path,
                                               self.config.get("Screens", "screen_countdown_1",
                                                               fallback="ScreenCountdown1.png"))
        self.screen_countdown_2 = os.path.join(self.screens_abs_file_path,
                                               self.config.get("Screens", "screen_countdown_2",
                                                               fallback="ScreenCountdown2.png"))
        self.screen_countdown_3 = os.path.join(self.screens_abs_file_path,
                                               self.config.get("Screens", "screen_countdown_3",
                                                               fallback="ScreenCountdown3.png"))
        self.screen_countdown_4 = os.path.join(self.screens_abs_file_path,
                                               self.config.get("Screens", "screen_countdown_4",
                                                               fallback="ScreenCountdown4.png"))
        self.screen_countdown_5 = os.path.join(self.screens_abs_file_path,
                                               self.config.get("Screens", "screen_countdown_5",
                                                               fallback="ScreenCountdown5.png"))
        self.screen_black = os.path.join(self.screens_abs_file_path,
                                         self.config.get("Screens", "screen_black",
                                                         fallback="ScreenBlack.png"))
        self.screen_print = os.path.join(self.screens_abs_file_path,
                                         self.config.get("Screens", "screen_print",
                                                         fallback="ScreenPrint.png"))
        self.screen_change_ink = os.path.join(self.screens_abs_file_path,
                                              self.config.get("Screens", "screen_change_ink",
                                                              fallback="ScreenChangeInk.png"))
        self.screen_change_paper = os.path.join(self.screens_abs_file_path,
                                                self.config.get("Screens", "screen_change_paper",
                                                                fallback="ScreenChangePaper.png"))


    def is_photo_taken(self):
        return self.photo_taken

    def setCameraColor(self, color):
        if color == "bw":
            self.camera.color_effects = (128, 128)  # turn camera to black and white
        elif color == "sepia":
            self.camera.color_effects = (100, 150)  # turn camera to black and white
        else:
            self.camera.color_effects = None


    # Button1 callback function. Actions depends on state of the Photobooth state machine

    def Button1pressed(self, event):
        logging.debug("Button1pressed")
        time_now = time.time()

        #if button was pressed
        if self.button1active:
            if (time_now - self.time_stamp_button1) < 3.0:
                return

        self.button1active = True

        # wait until button is released
        while not GPIO.input(self.pin_button_up):
            time.sleep(0.1)
            # if button pressed longer than 5 sec -> shutdown
            if (time.time() - time_now) > 5:
                subprocess.call("sudo poweroff", shell=True)
                return

        time.sleep(0.2)

        # if in PowerOnState - ignore Buttons
        if self.state == "PowerOn":
            return

        # if in PrintPhoto State - ignore Buttons
        if self.state == "PrintPhoto":
            return

        # debounce the button
        if (time_now - self.time_stamp_button1) >= 0.5:
            logging.debug("Debounce time reached")

            logging.debug("self.button1 start")
            self.Button1()
            logging.debug("self.button1 ready -> Set new TimeStamp")
            self.time_stamp_button1 = time.time()
            self.button1active = False
            self.button2active = False

    # Button2 callback function. Actions depends on state of the Photobooth state machine
    def Button2pressed(self, event):
        logging.debug("Button2pressed")
        time_now = time.time()

        if self.button2active:
            if (time_now - self.time_stamp_button2) < 3.0:
                return

        self.button2active = True

        # wait until button is released
        while not GPIO.input(self.pin_button_down):
            time.sleep(0.1)
            # if button pressed longer than 5 sec -> exit program
            if (time.time() - time_now) > 5:
                sys.exit()
                return

        time.sleep(0.2)

        # if in PowerOnState - ignore Buttons
        if self.state == "PowerOn":
            return

        # if in PrintPhoto State - ignore Buttons
        if self.state == "PrintPhoto":
            return

        # debounce the button
        if (time_now - self.time_stamp_button2) >= 0.5:
            logging.debug("Debounce time reached")
            
            logging.debug("self.button2 start")
            self.Button2()
            logging.debug("self.button2 ready -> Set new TimeStamp")
            self.time_stamp_button2 = time.time()
            self.button1active = False
            self.button2active = False

    # This function captures the photo
    def taking_photo(self):
        logging.debug("Taking Photo")
        # get the filename also for later use
        base_filename = self.get_base_filename_for_images()
        self.lastfilename = base_filename + '.jpg'
        last_photoshow_filename = base_filename + '_photoshow.jpg'

        ## take a picture
        self.camera.capture(self.lastfilename)
        self.camera.capture(last_photoshow_filename, resize=(self.screen_w, self.screen_h))
        logging.debug("Photo saved: " + self.lastfilename)
        self.photo_taken = True

    # Power On Check State
    # check if printer is connected and turned on
    def on_enter_PowerOn(self):
        logging.debug("now on_enter_PowerOn")
        self.overlay_screen_turnOnPrinter = -1

        if not self.CheckPrinter():
            logging.debug("no printer found")
            self.overlay_screen_turnOnPrinter = self.overlay_image_transparency(self.screen_turnOnPrinter, 0, 3)

        while not self.CheckPrinter():
            time.sleep(2)

        logging.debug("printer found")
        self.PrinterFound()

    # leave Power On Check State
    def on_exit_PowerOn(self):
        logging.debug("now on_exit_PowerOn")

        # remove overlay "turn on printer", if still on display
        self.remove_overlay(self.overlay_screen_turnOnPrinter)

    # Start State -> Show initial Screen
    def on_enter_Start(self):
        self.button1active = False
        self.button2active = False
        
        logging.debug("now on_enter_Start")
        self.overlay_screen_blackbackground = self.overlay_image(self.screen_black, 0, 2)
        self.overlay_screen_start = self.overlay_image_transparency(self.screen_start, 0, 7)

    # leave start screen
    def on_exit_Start(self):
        logging.debug("now on_exit_Start")
        self.remove_overlay(self.overlay_screen_start)

    # countdown to zero and take picture
    def on_enter_CountdownPhoto(self):
        logging.debug("now on_enter_CountdownPhoto")

        #set the picture color
        self.setCameraColor('')

        # print the countdown
        self.overlay_screen_Countdown = self.overlay_image_transparency(self.screen_countdown_5, 0, 7)
        time.sleep(1)
        self.remove_overlay(self.overlay_screen_Countdown)
        self.overlay_screen_Countdown = self.overlay_image_transparency(self.screen_countdown_4, 0, 7)
        time.sleep(1)
        self.remove_overlay(self.overlay_screen_Countdown)
        self.overlay_screen_Countdown = self.overlay_image_transparency(self.screen_countdown_3, 0, 7)
        time.sleep(1)
        self.remove_overlay(self.overlay_screen_Countdown)
        self.overlay_screen_Countdown = self.overlay_image_transparency(self.screen_countdown_2, 0, 7)
        time.sleep(1)
        self.remove_overlay(self.overlay_screen_Countdown)
        self.overlay_screen_Countdown = self.overlay_image_transparency(self.screen_countdown_1, 0, 7)
        time.sleep(1)
        self.remove_overlay(self.overlay_screen_Countdown)
        self.overlay_screen_Countdown = self.overlay_image_transparency(self.screen_countdown_0, 0, 7)
        time.sleep(1)
        self.remove_overlay(self.overlay_screen_Countdown)

        # countdown finished
        self.CountdownPhotoTimeout()

    def on_exit_CountdownPhoto(self):
        logging.debug("now on_exit_CountdownPhoto")

    # take a picture
    def on_enter_TakePhoto(self):
        logging.debug("now on_enter_TakePhoto")
        self.taking_photo()
        if self.printPicsEnable == True:
            self.to_ShowPrintPhoto()
        else:
            self.to_ShowPhoto()

    def on_exit_TakePhoto(self):
        logging.debug("now on_exit_TakePhoto")
        # turn off camera preview
        self.stoppreview()

    # show the picture with print option
    def on_enter_ShowPhoto(self):
        logging.debug("now on_enter_ShowPhoto")

        # show last photo and menu
        self.overlay_screen_black = self.overlay_image(self.screen_black, 0, 5)
        self.overlay_last_photo = self.overlay_image(self.lastfilename, 0, 6)

        # log filename
        logging.debug(str(self.lastfilename))

        # copy photo to USB Drive
        if self.PhotoCopyPath is not None:
            logging.debug(str(self.PhotoCopyPath))
            logging.debug(os.path.basename(str(self.lastfilename)))
            logging.debug((str(self.PhotoCopyPath)) + '/' + os.path.basename(str(self.lastfilename)))
            shutil.copyfile((str(self.lastfilename)),
                            ((str(self.PhotoCopyPath)) + '/' + os.path.basename(str(self.lastfilename))))

        logging.debug("start resizing")

        self.overlay_screen_print = self.overlay_image_transparency(self.screen_start, 0, 7)

        logging.debug("finish resizing")
        
    # state show photo
    def on_exit_ShowPhoto(self):
        logging.debug("now on_exit_ShowPhoto")
        self.startpreview()
        self.remove_overlay(self.overlay_screen_black)
        self.remove_overlay(self.overlay_last_photo)
        self.remove_overlay(self.overlay_screen_print)

    def after_Start_to_PhotoShow(self):
        logging.debug("now after_Start_to_PhotoShow")
        self.stoppreview()

    # state show photo show
    def on_enter_PhotoShow(self):
        logging.debug("now on_enter_PhotoShow")
        
        allPictures = [f for f in listdir(self.photo_abs_file_path) if isfile(join(self.photo_abs_file_path, f)) and f.endswith('_photoshow.jpg')]
        logging.debug(str(allPictures))
        logging.debug(len(allPictures))
        self.overlay_screen_black = -1
        self.overlay_last_photo = -1
        self.overlay_screen_print = -1
        self.photo_show_to_restart = False
        
        if len(allPictures) > 0:
            photo_file_name = self.photo_abs_file_path + allPictures[random.randint(0, len(allPictures)-1)]
            
            self.overlay_screen_black = self.overlay_image(self.screen_black, 0, 5)
            self.overlay_last_photo = self.overlay_image(photo_file_name, 0, 6)
            
            # log filename
            logging.debug(str(photo_file_name))
            
            logging.debug("start resizing")
            self.overlay_screen_print = self.overlay_image_transparency(self.screen_start, 0, 7)
            logging.debug("finish resizing")
        else:
            # should not be possible as prohibited in transition condition
            self.startpreview()
            self.to_Restart()
    
    def before_PhotoShow_to_Countdown(self):
        logging.debug("now before_PhotoShow_to_Countdown")
        self.startpreview()
    
    # state show photo
    def on_exit_PhotoShow(self):
        logging.debug("now on_exit_PhotoShow")
        self.remove_overlay(self.overlay_screen_black)
        self.remove_overlay(self.overlay_last_photo)
        self.remove_overlay(self.overlay_screen_print)

    # show the picture with print option
    def on_enter_ShowPrintPhoto(self):
        logging.debug("now on_enter_ShowPrintPhoto")

        # show last photo and menu
        self.overlay_screen_black = self.overlay_image(self.screen_black, 0, 5)
        self.overlay_last_photo = self.overlay_image(self.lastfilename, 0, 6)

        # log filename
        logging.debug(str(self.lastfilename))

        # copy photo to USB Drive
        if self.PhotoCopyPath is not None:
            logging.debug(str(self.PhotoCopyPath))
            logging.debug(os.path.basename(str(self.lastfilename)))
            logging.debug((str(self.PhotoCopyPath)) + '/' + os.path.basename(str(self.lastfilename)))
            shutil.copyfile((str(self.lastfilename)),
                            ((str(self.PhotoCopyPath)) + '/' + os.path.basename(str(self.lastfilename))))

        logging.debug("start resizing")

        self.overlay_screen_print = self.overlay_image_transparency(self.screen_print, 0, 7)

        logging.debug("finish resizing")

    # state show photo
    def on_exit_ShowPrintPhoto(self):
        logging.debug("now on_exit_ShowPrintPhoto")
        self.startpreview()
        self.remove_overlay(self.overlay_screen_black)
        self.remove_overlay(self.overlay_last_photo)
        self.remove_overlay(self.overlay_screen_print)

    # print the photo
    def on_enter_PrintPhoto(self):
        logging.debug("now on_enter_PrintPhoto")
        # restart camera
        self.stoppreview()
        self.startpreview()

        if self.printPicsEnable == False:
            logging.debug("print enable = false")

        # print photo?
        if self.printPicsEnable == True:
            logging.debug("print enable = true")

            # connect to cups
            conn = cups.Connection()
            printername = list(conn.getPrinters().keys())

            print(printername)
    
            # use first printer
            logging.debug("Printer Name: " + printername[0])
            conn.enablePrinter(printername[0])
    
            # check printer state
            printerstate = conn.getPrinterAttributes(printername[0], requested_attributes=["printer-state-message"])
    
            # if printer in error state ->
            if str(printerstate).find("error:") > 0:
                logging.debug(str(printerstate))
                conn.cancelAllJobs(printername[0], my_jobs=True, purge_jobs=True)
                if str(printerstate).find("06") > 0:
                    logging.debug("goto refill ink")
                    self.InkEmpty()
                    return
                if str(printerstate).find("03") > 0:
                    logging.debug("goto refill paper")
                    self.PaperEmpty()
                    return
                if str(printerstate).find("02") > 0:
                    logging.debug("goto refill paper")
                    self.PaperEmpty()
                    return
                else:
                    logging.debug("Printer error: unbekannt")
    
            # Send the picture to the printer
            conn.printFile(printername[0], self.lastfilename, "Photo Booth", {})
    
            # short wait
            time.sleep(5)
    
            stop = 0
            TIMEOUT = 60
    
            # Wait until the job finishes
            while stop < TIMEOUT:
                printerstate = conn.getPrinterAttributes(printername[0], requested_attributes=["printer-state-message"])
    
                if str(printerstate).find("error:") > 0:
                    logging.debug(str(printerstate))
                    if str(printerstate).find("06") > 0:
                        logging.debug("goto refill ink")
                        self.InkEmpty()
                        return
                    if str(printerstate).find("03") > 0:
                        logging.debug("goto refill paper")
                        self.PaperEmpty()
                        return
                    if str(printerstate).find("02") > 0:
                        logging.debug("goto refill paper")
                        self.PaperEmpty()
                        return
                    else:
                        logging.debug("Printer error: unbekannt")
    
                if printerstate.get("printer-state-message") is "":
                    logging.debug("printer-state-message = /")
                    break
                stop += 1
                time.sleep(1)
    
        else:
            logging.debug("Print disabled")

        self.PrintDone()

    def on_exit_PrintPhoto(self):
        logging.debug("now on_exit_PrintPhoto")

    # show refill paper instructions
    def on_enter_RefillPaper(self):
        logging.debug("now on_enter_RefillPaper")
        self.overlayscreen_refillpaper = self.overlay_image(self.screen_change_paper, 0, 8)

    def on_exit_RefillPaper(self):
        logging.debug("now on_exit_RefillPaper")
        self.remove_overlay(self.overlayscreen_refillpaper)

    # show refill ink instructions
    def on_enter_RefillInk(self):
        logging.debug("now on_enter_RefillInk")
        self.overlayscreen_refillink = self.overlay_image(self.screen_change_ink, 0, 8)

    def on_exit_RefillInk(self):
        logging.debug("now on_exit_RefillInk")
        self.remove_overlay(self.overlayscreen_refillink)

    # restart the programm -> restart camera to prevent memory leak of image overlay function!
    def on_enter_Restart(self):
        logging.debug("now on_enter_Restart")
        logging.debug("restart Camera")

        self.camera.close()

        # Setup Camera
        try:
            self.camera = picamera.PiCamera()
        except:
            logging.CRITICAL("error initializing the camera - exiting")
            raise SystemExit

        self.camera.resolution = (self.photo_w, self.photo_h)
        self.camera.hflip = self.flip_screen_h
        self.camera.vflip = self.flip_screen_v
        self.startpreview()

        # load the Logo of the Photobooth and display it
        self.overlayscreen_logo = self.overlay_image_transparency(self.screen_logo, 0, 5)

        self.to_Start()

    # start the camera
    def startpreview(self):
        logging.debug("Start Camera preview")
        self.camera.start_preview(resolution=(self.screen_w, self.screen_h))
        # camera.color_effects = (128, 128)  # SW
        pass

    # stop the camera
    def stoppreview(self):
        logging.debug("Stop Camera Preview")
        self.camera.stop_preview()
        pass

    # create filename based on date and time
    def get_base_filename_for_images(self):
        logging.debug("Get BaseName for ImageFiles")
        # returns the filename base
        base_filename = self.photo_abs_file_path + str(datetime.now()).split('.')[0]
        base_filename = base_filename.replace(' ', '_')
        base_filename = base_filename.replace(':', '-')

        logging.debug(base_filename)
        return base_filename

    # remove screen overlay
    def remove_overlay(self, overlay_id):
        # If there is an overlay, remove it
        logging.debug("Remove Overlay")
        logging.debug(overlay_id)
        if overlay_id != -1:
            self.camera.remove_overlay(overlay_id)

    # overlay one image on screen
    def overlay_image(self, image_path, duration=0, layer=3):
        # Add an overlay (and time.sleep for an optional duration).
        # If time.sleep duration is not supplied, then overlay will need to be removed later.
        # This function returns an overlay id, which can be used to remove_overlay(id).

        if not os.path.exists(image_path):
            logging.debug("Overlay Image path not found!")
            logging.debug(image_path)
            return -1

        logging.debug("Overlay Image")
        # Load the arbitrarily sized image
        img = Image.open(image_path)
        # Create an image padded to the required size with
        # mode 'RGB'
        pad = Image.new('RGB', (
            ((img.size[0] + 31) // 32) * 32,
            ((img.size[1] + 15) // 16) * 16,
        ))
        # Paste the original image into the padded one
        pad.paste(img, (0, 0))
        logging.debug("after padding")

        # Add the overlay with the padded image as the source,
        # but the original image's dimensions
        try:
            o_id = self.camera.add_overlay(pad.tobytes(), size=img.size)
            logging.debug("after overlay try 1")
        except AttributeError:
            o_id = self.camera.add_overlay(pad.tostring(), size=img.size)  # Note: tostring() is deprecated in PIL v3.x
            logging.debug("after overlay try 2")
        
        o_id.layer = layer

        logging.debug("Overlay ID = " + str(o_id))

        del img
        del pad

        if duration > 0:
            time.sleep(duration)
            self.camera.remove_overlay(o_id)
            return -1  # '-1' indicates there is no overlay
        else:
            return o_id  # we have an overlay, and will need to remove it later

    # overlay omage with transparency
    def overlay_image_transparency(self, image_path, duration=0, layer=3):
        # Add an overlay (and time.sleep for an optional duration).
        # If time.sleep duration is not supplied, then overlay will need to be removed later.
        # This function returns an overlay id, which can be used to remove_overlay(id).

        if not os.path.exists(image_path):
            logging.debug("Overlay Image path not found!")
            logging.debug(image_path)
            return -1

        logging.debug("Overlay Transparency Image")
        logging.debug(image_path)
        # Load the arbitrarily sized image
        img = Image.open(image_path)
        # Create an image padded to the required size with
        # mode 'RGB'
        pad = Image.new('RGBA', (
            ((img.size[0] + 31) // 32) * 32,
            ((img.size[1] + 15) // 16) * 16,
        ))
        # Paste the original image into the padded one
        pad.paste(img, (0, 0), img)

        # Add the overlay with the padded image as the source,
        # but the original image's dimensions
        try:
            o_id = self.camera.add_overlay(pad.tobytes(), size=img.size)
        except AttributeError:
            o_id = self.camera.add_overlay(pad.tostring(), size=img.size)  # Note: tostring() is deprecated in PIL v3.x

        o_id.layer = layer

        logging.debug("Overlay ID = " + str(o_id))

        del img
        del pad

        if duration > 0:
            time.sleep(duration)
            self.camera.remove_overlay(o_id)
            return -1  # '-1' indicates there is no overlay
        else:
            return o_id  # we have an overlay, and will need to remove it later

    # get the usb drive mount point
    def GetMountpoint(self):
        logging.debug("Get USB Drive Mount Point")
        try:
            context = pyudev.Context()
            removable = [device for device in context.list_devices(subsystem='block', DEVTYPE='disk') if
                         device.attributes.asstring('removable') == "1"]

            partitions = [removable[0].device_node for removable[0] in
                          context.list_devices(subsystem='block', DEVTYPE='partition', parent=removable[0])]
            for p in psutil.disk_partitions():
                if p.device in partitions:
                    logging.debug("Mountpoint = " + p.mountpoint)
                    return p.mountpoint

        except:
            logging.debug("No Drive Found")
            return None

    # check if the printer is connected and turned on
    def CheckPrinter(self):
        logging.debug("CheckPrinter")

        if self.printPicsEnable == False:
            logging.debug("printing disabled")
            return True

        busses = usb.busses()
        for bus in busses:
            devices = bus.devices
            for dev in devices:
                if dev.idVendor == 1193:
                    logging.debug("Printer Found")
                    logging.debug("  idVendor: %d (0x%04x)" % (dev.idVendor, dev.idVendor))
                    logging.debug("  idProduct: %d (0x%04x)" % (dev.idProduct, dev.idProduct))
                    return True
        logging.debug("PrinterNotFound")
        return False


# Main Routine
def main():
    # start logging
    log_filename = str(datetime.now()).split('.')[0]
    log_filename = log_filename.replace(' ', '_')
    log_filename = log_filename.replace(':', '-')

    loggingfolder = REAL_PATH + "/Log/"

    if not os.path.exists(loggingfolder):
        os.mkdir(loggingfolder)

    # logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.DEBUG, filename=REAL_PATH+"/test.log")
    logging.basicConfig(format='%(asctime)s-%(module)s-%(funcName)s:%(lineno)d - %(message)s', level=logging.DEBUG,
                        filename=loggingfolder + log_filename + ".log")
    logging.info("info message")
    logging.debug("debug message")

    while True:

        logging.debug("Starting Photobooth")

        photobooth = Photobooth()

        while True:
            time.sleep(0.1)
            pass

        #photobooth.__del__()


if __name__ == "__main__":
    try:
        main()

    except KeyboardInterrupt:
        logging.debug("keyboard interrupt")

    except Exception as exception:
        logging.critical("unexpected error: " + str(exception))

    finally:
        logging.debug("logfile closed")
